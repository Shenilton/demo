package com.finance.webAutomation;

import org.testng.annotations.Test;

import com.citi.mobileAutomation.dataProviders.DataProvider_Finance_ey;
import com.citi.mobileAutomation.financeApplication.Action;

public class Test_Finance_EY extends DataProvider_Finance_ey {

	Action action = new Action();

	@Test(dataProvider = "ey_fin")
	public void test_applince(String Username, String pwd) throws InterruptedException {
		action.userName(Username).password(pwd);
		action.login();
	//	action.expandAll();
	}

}
