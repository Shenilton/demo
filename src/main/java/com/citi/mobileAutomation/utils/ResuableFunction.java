package com.citi.mobileAutomation.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.citi.mobileAutomation.controlers.InitMethodWindows;
import com.citi.mobileAutomation.controlers.Initilizer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.DeviceActionShortcuts;
import io.appium.java_client.InteractsWithApps;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ScrollsTo;
import io.appium.java_client.TouchAction;

public class ResuableFunction extends Initilizer {

	public static void implicitlyWaitTimeOut(int timeout) {
		try {
			driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public static boolean isExist(String Id) {
		try {
			driver.findElement(By.id(Id)).click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isExistXpath(String Id) {
		try {
			driver.findElement(By.xpath(Id)).click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isExistSend(String Id, String keysToSend) {
		try {
			driver.findElement(By.id(Id)).sendKeys(keysToSend);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static void clearForWaitByID(String value) throws InterruptedException {
		Thread.sleep(4000);
		driver.findElement(By.id(value)).clear();
	}

	public static void sendForWaitByXpath(String value) throws InterruptedException {
		Thread.sleep(4000);
		driver.getKeyboard();
		MobileElement element = (MobileElement) driver.findElementById(value);
		element.click();
		element.sendKeys("Mobiles2862");
	}

	public static void clcikForWaitByID(String value) throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id(value))).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clcikByID(String value) {
		try {
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			driver.findElement(By.id(value)).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clcikByXpath(String value) {
		try {
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			driver.findElement(By.xpath(value)).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clcikForWaitByXpath(String value) throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(value))).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Set<String> findDuplicates(List<String> listContainingDuplicates) {
		final Set<String> setToReturn = new HashSet<String>();
		final Set<String> set1 = new HashSet<String>();
		for (String yourInt : listContainingDuplicates) {
			if (!set1.add(yourInt)) {
				setToReturn.add(yourInt);
			}
		}
		return setToReturn;
	}

	public static boolean sortOrder(List<String> value) {
		String previous = "";
		value = new ArrayList<String>();
		for (final String current : value) {
			value.add(current);
			if (current.compareTo(previous) < 0)
				return false;
			previous = current;
		}
		return true;
	}

	public static boolean containsDuplication(List<String> list) {
		Set<String> set = new HashSet<String>(list);
		if (set.size() < list.size()) {
			return true;
		} else {
			return false;
		}
	}

	public static void sendByForWaitByID(String value, String value2) throws InterruptedException {
		Thread.sleep(4000);
		driver.findElement(By.id(value)).sendKeys(value2);
	}

	public static void sendByForWaitByXpath(String value, String value2) throws InterruptedException {
		Thread.sleep(4000);
		driver.findElement(By.xpath(value)).sendKeys(value2);
	}

	public static void sendByWaitByString(String locator, String value, String valueToBeSent) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		if (locator.equalsIgnoreCase("ID")) {
			driver.findElement(By.id(value)).clear();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(value))).sendKeys(valueToBeSent);
		} else if (locator.equalsIgnoreCase("xpath")) {
			driver.findElement(By.xpath(value)).clear();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value))).sendKeys(valueToBeSent);
		}
	}
	
	public static void sendByWaitByString_(String locator, String value, String valueToBeSent) {
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		if (locator.equalsIgnoreCase("ID")) {
			driver.findElement(By.id(value)).clear();
			driver.findElement(By.id(value)).sendKeys(valueToBeSent);
		} else if (locator.equalsIgnoreCase("xpath")) {
			driver.findElement(By.xpath(value)).clear();
			driver.findElement(By.xpath(value)).sendKeys(valueToBeSent);
		}
	}

	public static String getTextElementBy(String by) {
		MobileElement element = (MobileElement) driver.findElement(By.id(by));
		String returnValue = element.getText().toString();
		return returnValue;
	}

	public static void clickByClassName(String elements, int index) throws InterruptedException {
		List<WebElement> elementClick = driver.findElementsByClassName(elements);
		elementClick.get(index).click();
	}

	public static void touchTabXPATH(String element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		TouchAction ta = new TouchAction((MobileDriver) driver);
		MobileElement ele = (MobileElement) wait.until(ExpectedConditions.elementToBeClickable(By.xpath(element)));
		ta.tap(ele).perform();
	}

	public static void touchTabID(String element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		TouchAction ta = new TouchAction((MobileDriver) driver);
		MobileElement ele = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element)));
		ta.tap(ele).perform();
	}

	public static void touchTabName(String element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		TouchAction ta = new TouchAction((MobileDriver) driver);
		MobileElement ele = (MobileElement) wait
				.until(ExpectedConditions.visibilityOf(driver.findElementByName(element)));
		ta.tap(ele).perform();
	}

	/* ==================Newly added Function====================== */
	public static String getTextFromElementByWait(String identifier, String locator, int waitTime) {
		String returnValue = null;
		if (identifier.equalsIgnoreCase("id")) {
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			MobileElement element = (MobileElement) wait
					.until(ExpectedConditions.presenceOfElementLocated(By.id(locator)));
			returnValue = element.getText();
		} else if (identifier.equalsIgnoreCase("xpath")) {
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			MobileElement element = (MobileElement) wait
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
			returnValue = element.getText();
		} else if (identifier.equalsIgnoreCase("idAttribute")) {
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			MobileElement element = (MobileElement) wait
					.until(ExpectedConditions.presenceOfElementLocated(By.id(locator)));
			returnValue = element.getAttribute("name").trim();
		} else if (identifier.equalsIgnoreCase("xpathAttribute")) {
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			MobileElement element = (MobileElement) wait
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
			returnValue = element.getAttribute("name").trim();
		}

		return returnValue;
	}

	public boolean swipeToDirection_Android(MobileElement el, String direction) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			HashMap<String, String> swipeObject = new HashMap<String, String>();
			if (direction.equals("d")) {
				swipeObject.put("direction", "down");
			} else if (direction.equals("u")) {
				swipeObject.put("direction", "up");
			} else if (direction.equals("l")) {
				swipeObject.put("direction", "left");
			} else if (direction.equals("r")) {
				swipeObject.put("direction", "right");
			}
			swipeObject.put("element", el.getId());
			js.executeScript("mobile:swipe", swipeObject);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isDisplayedInDom(String identifier, String element) {
		boolean value = false;
		try {
			if (identifier.equalsIgnoreCase("id")) {
				WebDriverWait wait = new WebDriverWait(driver, 30);
				MobileElement elements = (MobileElement) wait
						.until(ExpectedConditions.presenceOfElementLocated(By.id(element)));
				value = elements.isDisplayed();
			}
			if (identifier.equalsIgnoreCase("xpath")) {
				WebDriverWait wait = new WebDriverWait(driver, 30);
				MobileElement elements = (MobileElement) wait
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
				value = elements.isDisplayed();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public static boolean isEnabledInDom(String identifier, String element) {
		boolean value = false;
		try {
			if (identifier.equalsIgnoreCase("id")) {
				WebDriverWait wait = new WebDriverWait(driver, 30);
				MobileElement elements = (MobileElement) wait
						.until(ExpectedConditions.presenceOfElementLocated(By.id(element)));
				value = elements.isEnabled();
			}
			if (identifier.equalsIgnoreCase("xpath")) {
				WebDriverWait wait = new WebDriverWait(driver, 30);
				MobileElement elements = (MobileElement) wait
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
				value = elements.isEnabled();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public void waitForElement(AppiumDriver<MobileElement> driver, long time, String element) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(element))).click();
	}

	@SuppressWarnings("deprecation")
	public static void scrollToClick(String text) throws InterruptedException {
		Thread.sleep(10000);
		try {
			((ScrollsTo<WebElement>) driver).scrollTo(text).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public static void scrollTo(String text) throws InterruptedException {
		Thread.sleep(6000);
		try {
			((ScrollsTo<WebElement>) driver).scrollTo(text);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static MobileElement returnMobileElement(String element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		MobileElement elements = (MobileElement) wait
				.until(ExpectedConditions.presenceOfElementLocated(By.id(element)));
		return elements;
	}

	public static MobileElement returnMobileElementXPATH(String element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		MobileElement elements = (MobileElement) wait
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
		return elements;
	}

	public static void longPress(MobileElement element) {
		TouchActions action = new TouchActions(driver);
		action.longPress(element);
		action.perform();
	}

	public static void selectingAnElementWithCordinates(int srcX_CO, int srcY_CO, int desX_CO, int desY_CO) {
		TouchAction ts = new TouchAction((MobileDriver) driver);
		ts.longPress(srcX_CO, srcY_CO).moveTo(desX_CO, desY_CO).release().perform();
	}

	public static void hiddingKeyBoard() {
		try {
			((DeviceActionShortcuts) driver).hideKeyboard();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean contains(int[] arr, int item) {
		for (int n : arr) {
			if (item == n) {
				return true;
			}
		}
		return false;
	}

	public static boolean waitElementIsDisplayed(String by) {
		boolean returnValue = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 70);
			returnValue = wait.until(ExpectedConditions.presenceOfElementLocated(By.id(by))).isDisplayed();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	public static Map<String, List<String>> splitFunction(List<String> value) {
		Map<String, List<String>> mapping = new HashMap<String, List<String>>();
		List<String> addingRefNumber = new ArrayList<String>();
		List<String> addingBillersName = new ArrayList<String>();
		String[] seperator = null;
		for (String firstValue : value) {
			seperator = firstValue.split("\\|");
			addingRefNumber.add(seperator[0].trim().toString());
			addingBillersName.add(seperator[1].trim().toString());
		}
		mapping.put("RefNumber", addingRefNumber);
		mapping.put("BillersName", addingBillersName);
		return mapping;
	}

	public static String todayDate() {
		String pattern = "dd MMM yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());
		return date;
	}

	public static String tommarowDate() {
		final SimpleDateFormat format = new SimpleDateFormat("EEEE, dd MMM yyyy");
		final Date date = new Date();
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		return format.format(calendar.getTime());
	}

	public static void clcikForWaitByXpathMobileElementsFromList(MobileElement ele) throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOf(ele)).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void tapWithCordinates(int x, int y) throws InterruptedException {
		try {
			TouchAction touchAction = new TouchAction((MobileDriver) driver);
			Thread.sleep(10000);
			touchAction.tap(x, y).release().perform();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void listOfElements(String action, String id, int index, String keysToSend)
			throws InterruptedException {
		WebDriverWait waitForFormLabel = new WebDriverWait(driver, 30);
		List<WebElement> myIput = driver.findElements(By.id(id));
		if (action.equals("snd") && action.equals("ID")) {
			waitForFormLabel.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id(id)));
			myIput.get(index).sendKeys(keysToSend);
		} else if (action.equals("clk") && action.equals("ID")) {
			waitForFormLabel.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id(id)));
			myIput.get(index).sendKeys(keysToSend);
		} else if (action.equals("get") && action.equals("ID")) {
			waitForFormLabel.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id(id)));
			myIput.get(index).sendKeys(keysToSend);
		}
	}

	public static void listOfElementsXPATH(String action, String id, int index, String keysToSend)
			throws InterruptedException {
		WebDriverWait waitForFormLabel = new WebDriverWait(driver, 30);
		List<WebElement> myIput = driver.findElements(By.xpath(id));
		if (action.equals("snd") && action.equals("XPATH")) {
			waitForFormLabel.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(id)));
			myIput.get(index).sendKeys(keysToSend);
		} else if (action.equals("clk") && action.equals("XPATH")) {
			waitForFormLabel.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(id)));
			myIput.get(index).sendKeys(keysToSend);
		} else if (action.equals("get") && action.equals("XPATH")) {
			waitForFormLabel.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(id)));
			myIput.get(index).sendKeys(keysToSend);
		}
	}

	public static void scroll() throws InterruptedException {
		Thread.sleep(10000);
		org.openqa.selenium.Dimension size = driver.manage().window().getSize();
		int width = (int) (size.width / 2);
		int startPoint = (int) (size.getHeight() * 0.70);
		int endPoint = (int) (size.getHeight() * 0.20);
		int duration = 2000;
		((AppiumDriver<MobileElement>) driver).swipe(width, startPoint, width, endPoint, duration);
	}

	public static void keyBoardTouchActions(String string) throws InterruptedException {
		int count = 0;
		String username = string;
		for (int num = 0; num <= username.length() - 1; num++) {
			Thread.sleep(2000);
			if (Character.isUpperCase(username.charAt(num))) {
				tapOnKeyboard(KeyBoardCoordinates.coordinate.get("Shift"));
				Thread.sleep(2000);
				tapOnKeyboard(
						KeyBoardCoordinates.coordinate.get(Character.toString(username.charAt(num)).toLowerCase()));
				System.out.println(Character.toString(username.charAt(num)).toLowerCase());
			} else if (Character.isDigit(username.charAt(num))) {
				if (count == 0) {
					Thread.sleep(2000);
					tapOnKeyboard(KeyBoardCoordinates.coordinate.get("Number"));
					tapOnKeyboard(KeyBoardCoordinates.coordinate.get(Character.toString(username.charAt(num))));
					System.out.println(Character.toString(username.charAt(num)));
					count++;
				} else if (Character.isLetter(username.charAt(num - 1))) {
					tapOnKeyboard(KeyBoardCoordinates.coordinate.get("Number"));
					tapOnKeyboard(KeyBoardCoordinates.coordinate.get(Character.toString(username.charAt(num))));
					System.out.println(Character.toString(username.charAt(num)));
				} else {
					tapOnKeyboard(KeyBoardCoordinates.coordinate.get(Character.toString(username.charAt(num))));
					System.out.println(Character.toString(username.charAt(num)));
				}
			} else if (!Character.isUpperCase(username.charAt(num)) && !Character.isDigit(username.charAt(num))
					&& !Character.isLetter(username.charAt(num))) {
				tapOnKeyboard(KeyBoardCoordinates.coordinate.get("Number"));
				tapOnKeyboard(KeyBoardCoordinates.coordinate.get("Shift"));
				tapOnKeyboard(KeyBoardCoordinates.coordinate.get(Character.toString(username.charAt(num))));
				tapOnKeyboard(KeyBoardCoordinates.coordinate.get("Number"));
				System.out.println(Character.toString(username.charAt(num)));
			} else {

				if (Character.isLetter(username.charAt(num))) {
					if (num == 0) {
						tapOnKeyboard(KeyBoardCoordinates.coordinate.get(Character.toString(username.charAt(num))));
						System.out.println(Character.toString(username.charAt(num)));
					} else if (Character.isDigit(username.charAt(num - 1))) {
						tapOnKeyboard(KeyBoardCoordinates.coordinate.get("Number"));
						tapOnKeyboard(KeyBoardCoordinates.coordinate.get(Character.toString(username.charAt(num))));
						System.out.println(Character.toString(username.charAt(num)));
					} else {
						tapOnKeyboard(KeyBoardCoordinates.coordinate.get(Character.toString(username.charAt(num))));
						System.out.println(Character.toString(username.charAt(num)));
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static void ScrollIOS(String input, String object, String elementToScrolled) {
		if (input.equalsIgnoreCase("xpath")) {
			MobileElement drawee = (MobileElement) driver.findElement(By.xpath(object));
			Actions act = new Actions(driver);
			act.clickAndHold(drawee);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			@SuppressWarnings("rawtypes")
			HashMap scrollObject = new HashMap<>();
			scrollObject.put("predicateString", "value == '" + elementToScrolled + "'");
			js.executeScript("mobile: scroll", scrollObject);
			act.release();
		}
		if (input.equalsIgnoreCase("id")) {
			MobileElement drawee = (MobileElement) driver.findElement(By.id(object));
			Actions act = new Actions(driver);
			act.clickAndHold(drawee);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			@SuppressWarnings("rawtypes")
			HashMap scrollObject = new HashMap<>();
			scrollObject.put("predicateString", "value == '" + elementToScrolled + "'");
			js.executeScript("mobile: scroll", scrollObject);
			act.release();
		}
	}

	@SuppressWarnings("unchecked")
	public static void ScrollIOSclick(String input, String object, String elementToScrolled,
			String elementToBeClickedAfterScrolling) {
		if (input.equalsIgnoreCase("xpath")) {
			MobileElement drawee = (MobileElement) driver.findElement(By.xpath(object));
			Actions act = new Actions(driver);
			act.clickAndHold(drawee);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			@SuppressWarnings("rawtypes")
			HashMap scrollObject = new HashMap<>();
			scrollObject.put("predicateString", "value == '" + elementToScrolled + "'");
			js.executeScript("mobile: scroll", scrollObject);
			act.release();
		}
		if (input.equalsIgnoreCase("id")) {
			MobileElement drawee = (MobileElement) driver.findElement(By.id(object));
			Actions act = new Actions(driver);
			act.clickAndHold(drawee);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			@SuppressWarnings("rawtypes")
			HashMap scrollObject = new HashMap<>();
			scrollObject.put("predicateString", "value == '" + elementToScrolled + "'");
			js.executeScript("mobile: scroll", scrollObject);
			act.release();
		}
	}

	public static void tapOnKeyboard(Point p) throws InterruptedException {
		TouchAction touch = new TouchAction((MobileDriver) driver);
		touch.tap(p.getX(), p.getY()).perform();
	}

	public static void clickOnAlert() {
		try {
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void scrolliOS(String text, String direction) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<Object, Object> scrollObject = new HashMap<>();
		scrollObject.put("predicateString", "value == '" + text + "'");
		scrollObject.put("direction", direction);
		js.executeScript("mobile: scroll", scrollObject);
		try {

			Thread.sleep(3000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	public static void installMultipleApp(String appPath) {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		((InteractsWithApps) driver).installApp(appPath);
	}

	public static void launchAppForiOSAndroid() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		((InteractsWithApps) driver).launchApp();
	}
	
	//CODE CHANGE

	public static void launchApp() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		((InteractsWithApps) driver).resetApp();
	}

	public static void resetApp() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		((InteractsWithApps) driver).launchApp();
	}
	

	public static boolean isExistData(String object1, String object2) {
		WebDriverWait wait = new WebDriverWait(driver, 40);

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(object1))).isDisplayed();
			return true;
		} catch (Exception e) {
			try {
				clcikForWaitByID(object2);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			return false;
		}
	}

	public static boolean isExistData_XPATH(String object1, String object2) {
		WebDriverWait wait = new WebDriverWait(driver, 40);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(object1))).isDisplayed();
			return true;
		} catch (Exception e) {
			try {
				clcikForWaitByXpath(object2);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			return false;
		}
	}

	public static void hideKeyboardAndroid() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		((AppiumDriver<MobileElement>) driver).hideKeyboard();
	}

	public String mapReturnStringForKeyAndroid(String key, String sheetName) throws IOException {
		String path = InitMethodWindows.KEYWORDS_SG;
		Xls_Reader read = new Xls_Reader();
		System.out.println(read.path);
		read = new Xls_Reader(read.path);
		Map<String, String> map = read.setMapData(sheetName, path);
		String value = map.get(key);
		return value;
	}

	public static void errorMessageTrack(String errorMessage) {
		ArrayList<String> list = new ArrayList<>();
		list.add("Generic error");
		list.add("Your User ID or password may be incorrect. Please try again. If you are still unable to log in, try resetting your password or call CitiPhone Banking for assistance.");
		list.add("Unable to Load");
		list.add("Sorry, we are unable to process your request right now. Please try again later.");
		try {
			for (String individualErrorMessage : list) {
				if (individualErrorMessage.equals(errorMessage)) {
					logger.log(LogStatus.WARNING, "Due to this error " + errorMessage + " Application is quit");
					driver.quit();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void tapOnaElementByCordinates(String element) {
		MobileElement ele = returnMobileElement(element);
		Point loc = ele.getLocation();
		int x = loc.getX();
		int y = loc.getY();
		((AppiumDriver<MobileElement>) driver).tap(x, ele, y);
	}	
}