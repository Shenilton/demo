package com.citi.mobileAutomation.utils;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

/**
 * @Author Shenilton
 * @Date 10-Sep-2017
 * @Time 11:06:48 AM
 */

public class ExplicitWaiting {
	public static AppiumDriver<MobileElement> driver;

	/* Select using Strings */
	/** To Wait Until Element to be Clickable */
	public static void explicitWaitElementToBeClickable(AppiumDriver<MobileElement> driver, String element, int time,
			String selector) {
		if (selector.equalsIgnoreCase("id")) {
			WebDriverWait clickableWait = new WebDriverWait(driver, time);
			clickableWait.until(ExpectedConditions.elementToBeClickable(By.id(element)));
		} else if (selector.equalsIgnoreCase("xpath")) {
			WebDriverWait clickableWait = new WebDriverWait(driver, time);
			clickableWait.until(ExpectedConditions.elementToBeClickable(By.xpath(element)));
		}
	}

	/** To Wait Until Element to be Selectable */
	public static void explicitWaitElementToBeSelected(AppiumDriver<MobileElement> driver, String element, int time,
			String selector) {
		if (selector.equalsIgnoreCase("id")) {
			WebDriverWait selectableWait = new WebDriverWait(driver, time);
			selectableWait.until(ExpectedConditions.elementToBeSelected(By.id(element)));
		} else if (selector.equalsIgnoreCase("xpath")) {
			WebDriverWait selectableWait = new WebDriverWait(driver, time);
			selectableWait.until(ExpectedConditions.elementToBeSelected(By.xpath(element)));
		}
	}

	/** To Wait Until Element has the text */
	@SuppressWarnings("deprecation")
	public static void explicitWaitTextToBePresentInElement(AppiumDriver<MobileElement> driver, String element,
			int time, String text, String selector, String action) {
		if (selector.equalsIgnoreCase("id")) {
			WebDriverWait textToBePresent = new WebDriverWait(driver, time);
			textToBePresent.until(ExpectedConditions.textToBePresentInElement(By.id(element), text));
		}
		if (selector.equalsIgnoreCase("xpath")) {
			WebDriverWait textToBePresent = new WebDriverWait(driver, time);
			textToBePresent.until(ExpectedConditions.textToBePresentInElement(By.xpath(element), text));
		}
	}

	/** To Wait Until Title contains the text */
	public static void explicitWaitTitleContains(AppiumDriver<MobileElement> driver, int time, String title) {
		WebDriverWait titleContains = new WebDriverWait(driver, time);
		titleContains.until(ExpectedConditions.titleContains(title));
	}

	/** To Wait Until Title is */
	public static void explicitWaitTitleIs(AppiumDriver<MobileElement> driver, int time, String title) {
		WebDriverWait titleIs = new WebDriverWait(driver, time);
		titleIs.until(ExpectedConditions.titleIs(title));
	}

	/** To Wait Until Element to be Visible text */
	public static String explicitWaitVisibilityOfElementText(AppiumDriver<MobileElement> driver, String element,
			int time, String selector) {
		String value = null;
		if (selector.equalsIgnoreCase("id")) {
			MobileElement ele = driver.findElement(By.id(element));
			WebDriverWait elementToBeVisible = new WebDriverWait(driver, time);
			value = elementToBeVisible.until(ExpectedConditions.visibilityOf(ele)).getText();
		} else if (selector.equalsIgnoreCase("xpath")) {
			MobileElement ele = driver.findElement(By.xpath(element));
			WebDriverWait elementToBeVisible = new WebDriverWait(driver, time);
			value = elementToBeVisible.until(ExpectedConditions.visibilityOf(ele)).getText();
		}
		return value;
	}

	/** To Wait Until Element to be Visible ID */
	public static void explicitWaitVisibilityOfElementID(AppiumDriver<MobileElement> driver, String element, int time,
			String selector) {
		if (selector.equalsIgnoreCase("id")) {
			MobileElement ele = driver.findElement(By.id(element));
			WebDriverWait elementToBeVisible = new WebDriverWait(driver, time);
			elementToBeVisible.until(ExpectedConditions.visibilityOf(ele)).click();
		} else if (selector.equalsIgnoreCase("xpath")) {
			MobileElement ele = driver.findElement(By.xpath(element));
			WebDriverWait elementToBeVisible = new WebDriverWait(driver, time);
			elementToBeVisible.until(ExpectedConditions.visibilityOf(ele)).click();
		}
	}

	/** To Wait Until Element is Selected */
	public static void explicitWaitSelectionStateToBe(AppiumDriver<MobileElement> driver, String element, int time,
			boolean selected) {
		WebDriverWait elementIsSelected = new WebDriverWait(driver, time);
		elementIsSelected.until(ExpectedConditions.elementSelectionStateToBe(By.id(element), selected));
	}

	/** To Wait Until Elements to be Visible */
	public static List<WebElement> explicitWaitVisibilityOfElements(AppiumDriver<MobileElement> driver, String element,
			int time, String selector) {
		List<WebElement> ele = null;
		if (selector.equalsIgnoreCase("id")) {
			WebDriverWait elementsToBeVisible = new WebDriverWait(driver, time);
			ele = elementsToBeVisible.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id(element)));
		}
		return ele;
	}

	/* Select using By Method */
	/** To Wait Until Element to be Clickable */
	public static void explicitWaitElementToBeClickable(AppiumDriver<MobileElement> driver, String element, int time) {
		WebDriverWait clickableWait = new WebDriverWait(driver, time);
		clickableWait.until(ExpectedConditions.elementToBeClickable(By.id(element))).click();
	}

	/** To Wait Until Element to be Selectable */
	public static void explicitWaitElementToBeSelected(AppiumDriver<MobileElement> driver, By element, int time) {
		WebDriverWait selectableWait = new WebDriverWait(driver, time);
		selectableWait.until(ExpectedConditions.elementToBeSelected(element));
	}

	/** To Wait Until Title contains the text */
	public static void explicitWaitTitleContains(AppiumDriver<MobileElement> driver, By element, int time,
			String title) {
		WebDriverWait titleContains = new WebDriverWait(driver, time);
		titleContains.until(ExpectedConditions.titleContains(title));
	}

	/** To Wait Until Title is */
	public static void explicitWaitTitleIs(AppiumDriver<MobileElement> driver, By element, int time, String title) {
		WebDriverWait titleIs = new WebDriverWait(driver, time);
		titleIs.until(ExpectedConditions.titleIs(title));
	}

	/** To Wait Until Element to be Visible */
	public static void explicitWaitVisibilityOfElement(AppiumDriver<MobileElement> driver, By element, int time) {
		WebDriverWait elementToBeVisible = new WebDriverWait(driver, time);
		elementToBeVisible.until(ExpectedConditions.visibilityOfElementLocated(element));
	}

	/** To Wait Until Element is Selected */
	public static void explicitWaitSelectionStateToBe(AppiumDriver<MobileElement> driver, By element, int time,
			boolean selected) {
		WebDriverWait elementToBeVisible = new WebDriverWait(driver, time);
		elementToBeVisible.until(ExpectedConditions.elementSelectionStateToBe(element, selected));
	}

	/** To Wait for the Alert */
	public static void explicitWaitForAlert(AppiumDriver<MobileElement> driver, int time) {
		WebDriverWait waitForAlert = new WebDriverWait(driver, time);
		waitForAlert.until(ExpectedConditions.alertIsPresent());
	}
}
