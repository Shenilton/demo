package com.citi.mobileAutomation.utils;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Point;

public class KeyBoardCoordinates {
	public static Map<String, Point> coordinate;
	static {
		coordinate = new HashMap<String, Point>();
		coordinate.put("q", new Point(25, 525));
		coordinate.put("w", new Point(55, 525));
		coordinate.put("e", new Point(100, 525));
		coordinate.put("r", new Point(150, 525));
		coordinate.put("t", new Point(200, 525));
		coordinate.put("y", new Point(240, 525));
		coordinate.put("u", new Point(270, 525));
		coordinate.put("i", new Point(325, 525));
		coordinate.put("o", new Point(360, 525));
		coordinate.put("p", new Point(400, 525));

		coordinate.put("a", new Point(39, 568));
		coordinate.put("s", new Point(70, 570));
		coordinate.put("d", new Point(120, 585));
		coordinate.put("f", new Point(157, 595));
		coordinate.put("g", new Point(180, 590));
		coordinate.put("h", new Point(245, 580));
		coordinate.put("j", new Point(275, 585));
		coordinate.put("k", new Point(323, 582));
		coordinate.put("l", new Point(365, 585));

		coordinate.put("z", new Point(70, 644));
		coordinate.put("x", new Point(45, 525));
		coordinate.put("c", new Point(147, 644));
		coordinate.put("v", new Point(194, 644));
		coordinate.put("b", new Point(246, 644));
		coordinate.put("n", new Point(304, 644));
		coordinate.put("m", new Point(320, 644));

		coordinate.put("1", new Point(19, 525));
		coordinate.put("2", new Point(55, 525));
		coordinate.put("3", new Point(91, 525));
		coordinate.put("4", new Point(131, 525));
		coordinate.put("5", new Point(171, 525));
		coordinate.put("6", new Point(209, 525));
		coordinate.put("7", new Point(255, 525));
		coordinate.put("8", new Point(303, 525));
		coordinate.put("9", new Point(345, 525));
		coordinate.put("0", new Point(393, 525));

		coordinate.put("–", new Point(12, 582));
		coordinate.put("/", new Point(47, 582));
		coordinate.put(":", new Point(92, 582));
		coordinate.put(";", new Point(126, 582));
		coordinate.put("(", new Point(185, 582));
		coordinate.put(")", new Point(214, 582));
		coordinate.put("$", new Point(269, 582));
		coordinate.put("&", new Point(309, 582));
		coordinate.put("@", new Point(356, 582));
		coordinate.put("“", new Point(392, 582));

		coordinate.put(".", new Point(73, 651));
		coordinate.put(",", new Point(150, 651));
		coordinate.put("?", new Point(194, 651));
		coordinate.put("!", new Point(250, 651));
		coordinate.put("‘", new Point(326, 651));

		coordinate.put("Number", new Point(25, 702));
		coordinate.put("Shift", new Point(21, 638));

		coordinate.put("[", new Point(19, 525));
		coordinate.put("]", new Point(45, 525));
		coordinate.put("{", new Point(91, 525));
		coordinate.put("}", new Point(131, 525));
		coordinate.put("#", new Point(171, 525));
		coordinate.put("%", new Point(209, 525));
		coordinate.put("^", new Point(255, 525));
		coordinate.put("*", new Point(303, 525));
		coordinate.put("+", new Point(345, 525));
		coordinate.put("=", new Point(393, 525));

		coordinate.put("_", new Point(12, 582));
		// coordinate.put("\",new Point(47,582));
		coordinate.put("|", new Point(92, 582));
		coordinate.put("_", new Point(12, 582));
		coordinate.put("~", new Point(126, 582));
		coordinate.put("<", new Point(185, 582));
		coordinate.put(">", new Point(214, 582));
		coordinate.put("¢", new Point(269, 582));
		coordinate.put("£", new Point(309, 582));
		coordinate.put("¥", new Point(356, 582));
		coordinate.put("●", new Point(392, 582));
	}
}