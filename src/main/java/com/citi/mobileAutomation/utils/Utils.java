package com.citi.mobileAutomation.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import com.citi.mobileAutomation.baseTest.DriversInitilization;

public class Utils extends  DriversInitilization{
	
	public static File mkDirCreation() {
		String fileName = new SimpleDateFormat("yyyyMMddHH").format(new Date());
		String directaryName = System.getProperty("user.dir") + "//" + "ConsolidatedReport" + "//" + fileName;
		File theDir = new File(directaryName);
		if (!theDir.exists()) {
			boolean result = false;
			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
		return theDir;
	}	
	
	public static String addScreenshot() {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String encodedBase64 = null;
		FileInputStream fileInputStreamReader = null;
		try {
			fileInputStreamReader = new FileInputStream(scrFile);
			byte[] bytes = new byte[(int) scrFile.length()];
			fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.encodeBase64(bytes));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "data:image/png;base64," + encodedBase64;
	}
	
	public static File apkCall(String apkName) {
		File file = new File(System.getProperty("user.dir") + "/ListOfAPK's/" + apkName + ".apk".trim().toString());
		return file;

	}
}