package com.citi.mobileAutomation.dataMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class KeyWords_Engine {

	public static Map<String, List<String>> returnValue(String input) {
		Map<String, List<String>> mapping = new HashMap<String, List<String>>();
		List<String> actionList = new ArrayList<String>();
		List<String> objectList = new ArrayList<String>();
		for (String outer : splitBy(input, " || ")) {
			List<String> inner = splitBy(outer, "::");
			if (inner.size() == 2) {
				String action = inner.get(0);
				actionList.add(action);
				String elements = inner.get(1);
				objectList.add(elements);
				mapping.put("action", actionList);
				mapping.put("objects", objectList);
			}
		}
		return mapping;
	}

	private static List<String> splitBy(String toSplit, String delimiter) {
		List<String> tokens = new ArrayList<String>();
		StringTokenizer tokenizer = new StringTokenizer(toSplit, delimiter);
		while (tokenizer.hasMoreTokens()) {
			tokens.add(tokenizer.nextToken());
		}
		return tokens;
	}

	public static ArrayList<String> corDinateSplitter(String input) {
		System.out.println(input);
		ArrayList<String> list = new ArrayList<String>();
		String[] parts = input.split(",");
		String part1 = parts[0].trim(); 
		String part2 = parts[1].trim();
		list = new ArrayList<String>();
		list.add(part1);
		list.add(part2);
		return list;
	}
	
	public static ArrayList<String> multiOperationClick(String input) {
		System.out.println(input);
		ArrayList<String> list = new ArrayList<String>();
		String[] parts = input.split(":==:");
		String part1 = parts[0].trim(); 
		String part2 = parts[1].trim();
		list = new ArrayList<String>();
		list.add(part1);
		list.add(part2);
		return list;
	}
	
	public static void main(String[] args) {
		returnValue("clk::TRANSFERS_XPATH||clk::PT1_SOME_ONES_ID||clk::PT1_LCY_ACC_XPATH||clk::PT1_TAP_DROP_DOWN_ID||clk::PT1_FROM_LCY_AC_XPATH||snd::PT1_LCY_AMOUNT_FIELD_XPATH||hideKeyboard::NA||thread::SLEEP_TH||clk::PT1_NEXT_BUTTON_ID||clk::PT1_PAY_ID");
	}
}