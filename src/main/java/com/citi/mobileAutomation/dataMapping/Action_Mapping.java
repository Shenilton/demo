package com.citi.mobileAutomation.dataMapping;

import java.util.ArrayList;

import com.citi.mobileAutomation.assertEngine.Assert_Engine;
import com.citi.mobileAutomation.utils.ResuableFunction;

public class Action_Mapping {

	public static void actionMapping_Mobile_Application_Native_Android(String action, String ele, String object,
			String sentValue, String assertType, String expected, int indexForMultipleElements)
			throws InterruptedException {
		if (action.equalsIgnoreCase("clk") && ele.contains("XPATH")) {
			ResuableFunction.clcikForWaitByXpath(object);
		} else if (action.equalsIgnoreCase("clk") && ele.contains("ID")) {
			ResuableFunction.clcikForWaitByID(object);
		} else if (action.equalsIgnoreCase("snd") && ele.contains("XPATH")) {
			ResuableFunction.sendByWaitByString("Xpath", object, sentValue);
		} else if (action.equalsIgnoreCase("snd") && ele.contains("ID")) {
			ResuableFunction.sendByWaitByString("ID", object, sentValue);
		} else if (action.equalsIgnoreCase("sndIOS") && ele.contains("XPATH")) {
			ResuableFunction.clcikByXpath(object);
			ResuableFunction.keyBoardTouchActions(sentValue);
		} else if (action.equalsIgnoreCase("sndIOS") && ele.contains("ID")) {
			ResuableFunction.clcikByXpath(object);
			ResuableFunction.sendByWaitByString("Xpath", object, sentValue);
		} else if (action.equalsIgnoreCase("get") && ele.contains("ID")) {
			String assertByTextID = ResuableFunction.getTextFromElementByWait("id", object, 20);
			Assert_Engine.assertMapping(assertType, assertByTextID, expected, false);
		} else if (action.equalsIgnoreCase("get") && ele.contains("XPATH")) {
			String assertByTextXPATH = ResuableFunction.getTextFromElementByWait("xpath", object, 20);
			Assert_Engine.assertMapping(assertType, assertByTextXPATH, expected, false);
		} else if (action.equalsIgnoreCase("getByAt") && ele.contains("ID")) {
			String assertByTextXPATH = ResuableFunction.getTextFromElementByWait("idAttribute", object, 20);
			Assert_Engine.assertMapping(assertType, assertByTextXPATH, expected, false);
		} else if (action.equalsIgnoreCase("getByAt") && ele.contains("XPATH")) {
			String assertByTextXPATH = ResuableFunction.getTextFromElementByWait("xpathAttribute", object, 20);
			Assert_Engine.assertMapping(assertType, assertByTextXPATH, expected, false);
		} else if (action.equalsIgnoreCase("scroll") && ele.contains("TXT")) {
			ResuableFunction.scrollTo(object);
		} else if (action.equalsIgnoreCase("scrollClick") && ele.contains("TXT")) {
			if (ObjectGroups.keyValueReturn("platformToBeTested", "PRE_REQ_FOR_TEST").equalsIgnoreCase("iOS")) {
				ResuableFunction.scrolliOS(sentValue, sentValue);
			} else {
				ResuableFunction.scrollToClick(sentValue);
			}
		} else if (action.equalsIgnoreCase("scrollDown") && ele.contains("NA")) {
			ResuableFunction.scroll();
		} else if (action.equalsIgnoreCase("displayed") && ele.contains("XPATH")) {
			boolean displayedInDomXPATH = false;
			displayedInDomXPATH = ResuableFunction.isDisplayedInDom("xpath", object);
			Assert_Engine.assertMapping(assertType, "", expected, displayedInDomXPATH);
		} else if (action.equalsIgnoreCase("enabled") && ele.contains("ID")) {
			boolean displayedInDomXPATH = false;
			displayedInDomXPATH = ResuableFunction.isEnabledInDom("id", object);
			Assert_Engine.assertMapping(assertType, "", expected, displayedInDomXPATH);
		} else if (action.equalsIgnoreCase("enabled") && ele.contains("XPATH")) {
			boolean displayedInDomXPATH = false;
			displayedInDomXPATH = ResuableFunction.isEnabledInDom("xpath", object);
			Assert_Engine.assertMapping(assertType, "", expected, displayedInDomXPATH);
		} else if (action.equalsIgnoreCase("displayed") && ele.contains("ID")) {
			boolean displayedInDomID = false;
			displayedInDomID = ResuableFunction.isDisplayedInDom("id", object);
			Assert_Engine.assertMapping(assertType, "", expected, displayedInDomID);
		} else if (action.equalsIgnoreCase("cord") && ele.contains("CD")) {
			ArrayList<String> value = KeyWords_Engine.corDinateSplitter(sentValue);
			int xCordinate = Integer.valueOf(value.get(0));
			int yCordinate = Integer.valueOf(value.get(1));
			System.out.println("CORDINATE " + xCordinate + "CORDINATE " + yCordinate);
			ResuableFunction.tapWithCordinates(xCordinate, yCordinate);
		} else if (action.equalsIgnoreCase("alert") && ele.contains("NA")) {
			ResuableFunction.clickOnAlert();
		}/* else if (action.equalsIgnoreCase("resetLaunch") && ele.contains("NA")) {
			ResuableFunction.resetApp();
			ResuableFunction.launchApp();
		} */else if (action.equalsIgnoreCase("thread") && ele.contains("NA")) {
			long value = Long.valueOf(sentValue);
			Thread.sleep(value);
		} else if (action.equalsIgnoreCase("exist") && ele.contains("ID")) {
			ArrayList<String> value = KeyWords_Engine.multiOperationClick(sentValue);
			String firstValue = value.get(0);
			String secondValue = value.get(1);
			ResuableFunction.isExistData(firstValue, secondValue);
		} else if (action.equalsIgnoreCase("exist") && ele.contains("ID")) {
			ArrayList<String> value = KeyWords_Engine.multiOperationClick(sentValue);
			String firstValue = value.get(0);
			String secondValue = value.get(1);
			ResuableFunction.isExistData(firstValue, secondValue);
		} else if (action.equalsIgnoreCase("exist") && ele.contains("XPATH")) {
			ArrayList<String> value = KeyWords_Engine.multiOperationClick(sentValue);
			String firstValue = value.get(0);
			String secondValue = value.get(1);
			ResuableFunction.isExistData_XPATH(firstValue, secondValue);
		} else if (action.equalsIgnoreCase("hideKeyboard") && ele.contains("NA")) {
			ResuableFunction.hideKeyboardAndroid();
		} else if (action.equalsIgnoreCase("errorMessageValidation") && ele.contains("SPL_ID")) {
			String assertByTextXPATH = ResuableFunction.getTextFromElementByWait("id", object, 20);
			ResuableFunction.errorMessageTrack(assertByTextXPATH);
		} else if (action.equalsIgnoreCase("errorMessageValidation") && ele.contains("SPL_XPATH")) {
			String assertByTextXPATH = ResuableFunction.getTextFromElementByWait("xpath", object, 20);
			ResuableFunction.errorMessageTrack(assertByTextXPATH);
		}else if (action.equalsIgnoreCase("tap") && ele.contains("XPATH")) {
			ResuableFunction.clcikByID(object);
		}
	}
}