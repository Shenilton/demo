package com.citi.mobileAutomation.dataMapping;

public class DataImp {
	
	public static String preReqsite(String key) {
	  return ObjectGroups.keyValueReturn(key, "PRE_REQ_FOR_TEST");
	}
	
	public static String testData(String key) {
		  return ObjectGroups.keyValueReturn(key, "TEST_DATA");
		}
}
