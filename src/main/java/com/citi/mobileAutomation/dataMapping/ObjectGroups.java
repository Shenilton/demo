package com.citi.mobileAutomation.dataMapping;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.citi.mobileAutomation.controlers.CommonCallFunction;
import com.citi.mobileAutomation.utils.ResuableFunction;

public class ObjectGroups {

	public static void actionOnElements(String OBJECT_GROUP, String OBJECT_KEY, String ACTION_KEY,
			String ASSERTION_TYPE, String ASERTION_VALUE, String groupPath, String objectReprositaryPath,
			String inputToUI, String CONTROL_FLAG) throws InterruptedException {
		String inputData = "";
		int index = 0;
		try {
			if (OBJECT_GROUP.equalsIgnoreCase("groups") && CONTROL_FLAG.equalsIgnoreCase("Y")) {
				Map<String, List<String>> value = KeyWords_Engine.returnValue(
						CommonCallFunction.callPropertyValueInputDataWithPathObject(OBJECT_KEY, groupPath));
				System.out.println("----------Object CLL------- " + value);
				int count = 0;
				for (String action : value.get("action")) {
					String element = value.get("objects").get(count);
					String objectCall = CommonCallFunction.callPropertyValueInputDataWithPathObject(element,
							objectReprositaryPath);
					System.out.println("----------Object CLL------- " + element);
					System.out.println("----------Object CLL------- " + objectCall);
					count++;
					if (action.equalsIgnoreCase("cord")) {
						inputData = keyValueReturn(element, inputToUI);
						System.out.println("CORDINATE " + inputData);
						Action_Mapping.actionMapping_Mobile_Application_Native_Android(action, element, objectCall,
								inputData, ASSERTION_TYPE, ASERTION_VALUE, index);

					} else if (action.equalsIgnoreCase("snd")) {
						inputData = keyValueReturn(element, inputToUI);
						System.out.println("Shenilton "+inputData);
						Action_Mapping.actionMapping_Mobile_Application_Native_Android(action, element, objectCall,
								inputData, ASSERTION_TYPE, ASERTION_VALUE, index);

					} else if (action.equalsIgnoreCase("sndIOS")) {
						inputData = keyValueReturn(OBJECT_KEY, inputToUI);
						Action_Mapping.actionMapping_Mobile_Application_Native_Android(action, element, objectCall,
								inputData, ASSERTION_TYPE, ASERTION_VALUE, index);

					} else if (action.equalsIgnoreCase("tap")) {
						Action_Mapping.actionMapping_Mobile_Application_Native_Android(action, element, objectCall,
								inputData, ASSERTION_TYPE, ASERTION_VALUE, index);

					} else if (action.equalsIgnoreCase("scrollClick")) {
						inputData = keyValueReturn(element, inputToUI);
						if (ObjectGroups.keyValueReturn("platformToBeTested", "PRE_REQ_FOR_TEST")
								.equalsIgnoreCase("iOS")) {
							ResuableFunction.scrolliOS(objectCall, inputData);
						} else {
							ResuableFunction.scrollToClick(objectCall);
						}
					} else {
						Action_Mapping.actionMapping_Mobile_Application_Native_Android(action, element, objectCall,
								inputData, ASSERTION_TYPE, ASERTION_VALUE, index);
					}
				}
			} else if (OBJECT_GROUP.equalsIgnoreCase("single") && CONTROL_FLAG.equalsIgnoreCase("Y")) {
				inputData = keyValueReturn(OBJECT_KEY, inputToUI);
				String objectCall = CommonCallFunction.callPropertyValueInputDataWithPathObject(OBJECT_KEY,
						objectReprositaryPath);
				System.out.println("Object Key : " + OBJECT_KEY);
				System.out.println("Object value  : " + objectCall);
				Action_Mapping.actionMapping_Mobile_Application_Native_Android(ACTION_KEY, OBJECT_KEY, objectCall,
						inputData, ASSERTION_TYPE, ASERTION_VALUE, index);
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public static String keyValueReturn(String key, String SheetName) {
		ResuableFunction reuse = new ResuableFunction();
		String value = "";
		try {
			value = reuse.mapReturnStringForKeyAndroid(key, SheetName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return value;
	}

	public static String objectReproReturn(String key, String SheetName) {
		ResuableFunction reuse = new ResuableFunction();
		String value = "";
		try {
			value = reuse.mapReturnStringForKeyAndroid(key, SheetName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return value;
	}
}