package com.citi.mobileAutomation.specialscripts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ExtendReportImp {

	public static ExtentReports extent;
	public static ITestResult result;
	public static ExtentTest logger;
	public static WebDriverWait wait = null;
	public static AppiumDriver<MobileElement> driver;
	public static FileInputStream is = null;
	public static FileOutputStream fos = null;
	public static Xls_Reader data = null;
	String appiumServiceUrl;
	static File app = null;
	protected static DesiredCapabilities caps = null;
	OpeningAppiumServer server = null;

	public static boolean startReport() {
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			extent = new ExtentReports(mkDirCreation() + "/" + dateFormat.format(new Date())+"Error_Message" + ".html", true);
			extent.addSystemInfo("Environment", "Mobile App Environment");
			extent.addSystemInfo("Host Name", "LOCALHOST").addSystemInfo("Environment", "MOBILE Automation Testing")
					.addSystemInfo("User Name", "CITI USER");
			extent.loadConfig(new File(System.getProperty("user.dir") + "/extend-config.xml"));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean endReport() throws InterruptedException {
		try {
			extent.flush();
			extent.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static File mkDirCreation() {
		String fileName = new SimpleDateFormat("yyyyMMddHH").format(new Date());
		String directaryName = System.getProperty("user.dir") + "/" + "ErrorLoggedInAPP" + "/" + fileName;
		File theDir = new File(directaryName);
		if (!theDir.exists()) {
			boolean result = false;
			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
		return theDir;
	}

}
