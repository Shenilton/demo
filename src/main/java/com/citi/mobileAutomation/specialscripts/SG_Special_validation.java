package com.citi.mobileAutomation.specialscripts;

import com.citi.mobileAutomation.utils.ResuableFunction;

public class SG_Special_validation {

	public static void clickWithTextName(String platForm, String object) {
		if (platForm.equalsIgnoreCase("Android")) {
			String value = "//android.widget.TextView[@text='" + object + "']";
			try {
				ResuableFunction.clcikForWaitByXpath(value);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} 

		if (platForm.equalsIgnoreCase("iOS")) {
			String value = "//XCUIElementTypeStaticText[@name='" + object + "']";
			try {
				ResuableFunction.clcikForWaitByXpath(value);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} 
	}

	public static String getWithTextName(String platForm, String object) {
		String assertByText = "";
		if (platForm.equalsIgnoreCase("Android")) {
			String value = "//android.widget.TextView[@text='" + object + "']";
			assertByText = ResuableFunction.getTextFromElementByWait("xpath", value, 20);
		}
		if (platForm.equalsIgnoreCase("iOS")) {
			String value = "//XCUIElementTypeStaticText[@name='" + object + "']";
			assertByText = ResuableFunction.getTextFromElementByWait("xpath", value, 20);
		}
		return assertByText;
	}

}
