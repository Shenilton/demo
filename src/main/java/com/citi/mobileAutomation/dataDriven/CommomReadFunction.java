package com.citi.mobileAutomation.dataDriven;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import com.citi.mobileAutomation.controlers.InitMethodWindows;

public class CommomReadFunction {

	public static InputStream loadResource(final String resourcePath) throws IOException {
		final URL url = CommomReadFunction.class.getResource(resourcePath);
		if (url == null)
			throw new IOException(resourcePath + ": resource not found");
		return url.openStream();
	}

	public static String returnPreviousFolderOfProjectDirectory(String fileName) {
		Path path = null;
		try {
			path = Paths.get(CommomReadFunction.class.getResource("..").toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		String value1 = path.getParent().getParent().getParent().getParent().getParent().getParent().toString();
		return value1+fileName;

	}


	@SuppressWarnings("finally")
	public static String getPropertyVlaue(String redturnValue, String path) {
		Properties prop = new Properties();
		InputStream input = null;
		String finalPropertyVlaue = null;
		try {
			input = new FileInputStream(path);
			prop.load(input);
			finalPropertyVlaue = prop.getProperty(redturnValue);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return finalPropertyVlaue;
		}
	}
	
	public static String propertyValueCall(String redturnValue, String filePath) {
		String value = getPropertyVlaue(redturnValue, returnPathValue(filePath));
		return value;
	}
	
	public static String returnPathValue(String fileLocalPath) {
		String finalVlaue = InitMethodWindows.ProjectWorkingDirectory + fileLocalPath.toString().trim();
		return finalVlaue;
	}
	
	public static void main(String[] args) {
		String value = returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
		System.out.println(value);
	}
}