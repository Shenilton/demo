package com.citi.mobileAutomation.dataProviders;

import java.io.IOException;

import org.testng.annotations.DataProvider;

import com.citi.mobileAutomation.controlers.CommonCallFunction;
import com.citi.mobileAutomation.controlers.InitMethodiOS;

public class DataProvider_Finance_ey {
	
	@DataProvider(name = "ey_fin")
	public Object[][] test_PayBills() throws IOException {
		return CommonCallFunction.dataRequiredToTest("TODO_History", InitMethodiOS.KEYWORDS_EY);
	}
	

}
