package com.citi.mobileAutomation.dataProviders;

import java.io.IOException;

import org.testng.annotations.DataProvider;

import com.citi.mobileAutomation.controlers.CommonCallFunction;
import com.citi.mobileAutomation.controlers.InitMethodWindows;
import com.citi.mobileAutomation.controlers.InitMethodiOS;
import com.citi.mobileAutomation.controlers.Initilizer;

public abstract class DataProvider_SG extends Initilizer {

	@DataProvider(name = "PDFCOnvert")
	public Object[][] test_PayBills() throws IOException {
		return CommonCallFunction.dataRequiredToTest("PDFCOnvert", InitMethodiOS.KEYWORDS_SG);
	}
}