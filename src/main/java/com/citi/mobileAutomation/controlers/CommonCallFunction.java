package com.citi.mobileAutomation.controlers;

import java.io.IOException;

import com.citi.mobileAutomation.dataDriven.CommomReadFunction;
import com.citi.mobileAutomation.dataDriven.DataInputProvider;

public class CommonCallFunction {

	/**
	 * @throws IOException 
	 * @Author Shenilton
	 * @Date sept 21 2018
	 */

	// Calling system config values from properties file
	public static String callPropertyValueSysConFig(String redturnValue) throws IOException {
		String value = CommomReadFunction.propertyValueCall(redturnValue, InitMethodWindows.SYSTEM_CONFIG);
		return value;
	}

	// Calling Obects from Object Repro
	public static String callPropertyValueObjectRepro(String redturnValue) throws IOException {
		String value = CommomReadFunction.propertyValueCall(redturnValue, InitMethodWindows.OBJECT_REPROSITORY);
		return value;
	}

	// Calling Group Objects from properties file
	public static String callPropertyValueObjectGroup(String redturnValue) throws IOException {
		String value = CommomReadFunction.propertyValueCall(redturnValue, InitMethodWindows.OBJECT_REPROSITORY_ANDROID);
		return value;
	}

	// Input data
	public static String callPropertyValueInputData(String redturnValue) throws IOException {
		String value = CommomReadFunction.propertyValueCall(redturnValue, InitMethodWindows.INPUT_DATA_ANDROID);
		return value;
	}
	
	public static String callPropertyValueInputDataWithPathDetails(String redturnValue, String propertyPath) throws IOException {
		String value = CommomReadFunction.propertyValueCall(redturnValue, propertyPath);
		return value;
	}
	
	public static String callPropertyValueInputDataWithPathObject(String redturnValue, String propertyPath) throws IOException {
		String value = CommomReadFunction.propertyValueCall(redturnValue, propertyPath);
		return value;
	}
	
	public static Object[][] dataRequiredToTest(String sheetName, String pathOfTheSheet) throws IOException {
		return DataInputProvider.getSheet(sheetName, pathOfTheSheet);
	}
	
	public static void main(String[] args) throws IOException {
		String value =callPropertyValueSysConFig("platformToBeTested");
		System.out.println(value);
	}
}