package com.citi.mobileAutomation.controlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.baseTest.BrowserFactory;
import com.citi.mobileAutomation.baseTest.DriversInitilization;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataMapping.DataImp;
import com.citi.mobileAutomation.dataMapping.ObjectGroups;
import com.citi.mobileAutomation.utils.ResuableFunction;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class Initilizer {

	public static ExtentReports extent;
	public static ITestResult result;
	public static ExtentTest logger;
	public static WebDriverWait wait = null;
	public static RemoteWebDriver driver;
	public static FileInputStream is = null;
	public static FileOutputStream fos = null;
	public static Xls_Reader data = null;
	String appiumServiceUrl;
	static File app = null;
	protected static DesiredCapabilities caps = null;
	OpeningAppiumServer server = null;

	public Initilizer() {
		AppiumDriver<MobileElement> driver;
	}

	@BeforeSuite()
	public static void appiumDriverInitialization() throws InterruptedException, IOException {
		// OpeningAppiumServer.startAppiumServer();
		if (DataImp.preReqsite("platformToBeTested").equalsIgnoreCase("iOS")) {
			driver = DriversInitilization.initilizeiOS("SG_UAT2_05Apr_DEBUG");
		} else if (DataImp.preReqsite("platformToBeTested").equalsIgnoreCase("Android")) {
			InvokingEmulator.startAvd();
			driver = DriversInitilization.initilizeAndroid("SG_21-Mar-19_UAT2_INT_H1416-UJ");
		} else if (DataImp.preReqsite("platformToBeTested").equalsIgnoreCase("web")) {
			try {
				driver = (RemoteWebDriver) BrowserFactory.createDriver(DataImp.preReqsite("BrowserTested"),
						DataImp.preReqsite("WebsiteURL"), 30, 30);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@AfterSuite
	public void closeOpenItems() throws IOException, InterruptedException {
		if (DataImp.preReqsite("platformToBeTested").equalsIgnoreCase("iOS")) {
			driver.quit();
			OpeningAppiumServer.stopAppiumServer();
		} else if (DataImp.preReqsite("platformToBeTested").equalsIgnoreCase("Android")) {
			((AppiumDriver<MobileElement>) driver).closeApp();
			driver.quit();
			InvokingEmulator.stopAvd();
			OpeningAppiumServer.stopAppiumServer();
		} else if (DataImp.preReqsite("platformToBeTested").equalsIgnoreCase("web")) {
			driver.quit();
		}
	}
	// code change

	/*@BeforeClass(alwaysRun = true)
	public void reset() {
		ResuableFunction.resetApp();
	}
	
	*/

	@BeforeClass
	public boolean startReport() {
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			extent = new ExtentReports(mkDirCreation() + "/" + dateFormat.format(new Date()) + ".html", true);
			extent.addSystemInfo("Environment", "Mobile App Environment");
			extent.addSystemInfo("Host Name", "LOCALHOST").addSystemInfo("Environment", "MOBILE Automation Testing")
					.addSystemInfo("User Name", "CITI USER");
			extent.loadConfig(new File(System.getProperty("user.dir") + "/extend-config.xml"));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@AfterClass
	public boolean endReport() throws InterruptedException {
		try {
			extent.flush();
			extent.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@AfterMethod
	public boolean getResult(ITestResult result) throws IOException {
		try {
			if (result.getStatus() == ITestResult.FAILURE) {
				logger.log(LogStatus.FAIL, "Test Case Failed is " + result.getName());
				logger.log(LogStatus.FAIL, "Test Case Failed is " + result.getThrowable());
				logger.log(LogStatus.FAIL, logger.addBase64ScreenShot(addScreenshot()));
			} else if (result.getStatus() == ITestResult.SKIP) {
				logger.log(LogStatus.SKIP, "Test Case Skipped is " + result.getName());
			} else if (result.getStatus() == ITestResult.SUCCESS) {
				try {
					logger.log(LogStatus.PASS, "Test Case passed is " + result.getName());
					logger.log(LogStatus.PASS, logger.addBase64ScreenShot(addScreenshot()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			try {
				extent.endTest(logger);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String addScreenshot() {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String encodedBase64 = null;
		FileInputStream fileInputStreamReader = null;
		try {
			fileInputStreamReader = new FileInputStream(scrFile);
			byte[] bytes = new byte[(int) scrFile.length()];
			fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.encodeBase64(bytes));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "data:image/png;base64," + encodedBase64;
	}

	public static File mkDirCreation() {
		String fileName = new SimpleDateFormat("yyyyMMddHH").format(new Date());
		String directaryName = System.getProperty("user.dir") + "/" + "ConsolidatedReport" + "/" + fileName;
		File theDir = new File(directaryName);
		if (!theDir.exists()) {
			boolean result = false;
			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
		return theDir;
	}
}