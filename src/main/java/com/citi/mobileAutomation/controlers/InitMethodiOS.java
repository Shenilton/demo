package com.citi.mobileAutomation.controlers;

import java.io.File;

import org.testng.ISuite;
import org.testng.ISuiteResult;

import com.citi.mobileAutomation.dataDriven.CommomReadFunction;

public class InitMethodiOS {

	public static String FS = File.separator;
	public static String OSName = System.getProperty("os.name");
	public static String OSArchitecture = System.getProperty("os.arch");
	public static String OSVersion = System.getProperty("os.version");
	public static String OSBit = System.getProperty("sun.arch.data.model");
	public static String ProjectWorkingDirectory = System.getProperty("user.dir");
	public static String SYSTEM_CONFIG = "/src/main/resources/com/citi/mobileAutomation/config/system_variables.properties";
	public static String TestData = "";
	public static String LoginDetails = "";
	public static String PropertiesFiles = "";
	public static String Reports = "";
	public static String Images = "";
	public static String Videos = "";
	public static String OUTPUT_FOLDER = "";
	public static String FILE_NAME = "Extent Report.html";
	public static ISuite suite;
	public static ISuiteResult res;
	public static String extendXML = "";
	public static String INPUT_DATA = "TEST_DATA"; 
	public static String OBJECT_GROUP = "/src/main/resources/com/citi/mobileAutomation/ObjectRepro/ObjectGroup.properties";
	public static String KEYWORDS_SG = ProjectWorkingDirectory+"/src/main/resources/com/citi/mobileAutomation/Keywords_Source/Data_table_SG.XLSX";
	public static String OBJECT_REPROSITORY= "/src/main/resources/com/citi/mobileAutomation/ObjectRepro/ObjectReprositary_SG.properties";
	public static String KEYWORDS_EY =ProjectWorkingDirectory+"/src/main/resources/com/citi/mobileAutomation/Keywords_Source/Sample_Data.xlsx";
}
