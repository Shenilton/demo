package com.citi.mobileAutomation.controlers;

import java.io.File;
import org.testng.ISuite;
import org.testng.ISuiteResult;

import com.citi.mobileAutomation.dataDriven.CommomReadFunction;

public class InitMethodWindows {
	
	/**
	 * @Author Shenilton
	 * @Date sept 21 2018
	 */

	public static final String FS = File.separator;
	public static final String OSName = System.getProperty("os.name");
	public static final String OSArchitecture = System.getProperty("os.arch");
	public static final String OSVersion = System.getProperty("os.version");
	public static final String OSBit = System.getProperty("sun.arch.data.model");
	public static final String ProjectWorkingDirectory = System.getProperty("user.dir");
	public static final String TEST_DATA = "/src/test/resources/com/citi/mobileAutomation/data_source/EMEA.xlsx";
	public static final String KEYWORDS = "/src/main/resources/com/citi/mobileAutomation/Keywords_Source/Data_table_ID.xlsx";
	public static final String SYSTEM_CONFIG = "/src/main/resources/com/citi/mobileAutomation/config/system_variables.properties";
	public static final String OBJECT_REPROSITORY = "/src/main/resources/com/citi/mobileAutomation/ObjectRepro/ObjectReprositary_SG.properties";
	public static final String OBJECT_GROUP_ANDROID = "/src/main/resources/com/citi/mobileAutomation/ObjectRepro/ObjectGroup.properties";
	public static final String INPUT_DATA = "/src/main/resources/com/citi/mobileAutomation/data_source/inputData.properties";
	public static final String Reports = "/src/test/resources/Reports/";
	public static final String Images = "/src/test/resources/Reports/Images/";
	public static final String OUTPUT_FOLDER = "/src/test/resources/Reports/";
	public static final String FILE_NAME = "Extent Report.html";
	public static ISuite suite;
	public static ISuiteResult res;
	public static final String SDK_PATH="";
	public static final String AVD_Emulator_PATH = "/emulator/emulator";
	public static final String AVD_PATH = "/platform-tools/adb";
	public static final String extendXML = "/extend-config.xml"; 
	public static final String OBJECT_REPROSITORY_ANDROID = "/src/main/resources/com/citi/mobileAutomation/ObjectRepro/ObjectReprositary_SG.properties";
	public static final String INPUT_DATA_ANDROID = "TEST_DATA";           
	public static final String KEYWORDS_SG = ProjectWorkingDirectory+"/src/main/resources/com/citi/mobileAutomation/Keywords_Source/Data_table_SG.XLSX";
	public static final String SHEET_NAME_ACCOUNT_DETAILS = "ACCOUNT_DETAILS_PAYMENTS";
	public static final String PRE_REQ= "PRE_REQ_FOR_TEST";
	}