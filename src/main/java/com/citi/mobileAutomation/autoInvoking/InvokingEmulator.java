package com.citi.mobileAutomation.autoInvoking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.citi.mobileAutomation.dataMapping.ObjectGroups;

public class InvokingEmulator extends Thread {
	
	/**
	 * @Author Shenilton
	 * @Date sept 21 2018
	 */
	
	private static final Logger LOGGER = Logger.getLogger(InvokingEmulator.class.getName());

	public static void startAvd() throws IOException, InterruptedException {
		try {
			ProcessBuilder pbEmulator = new ProcessBuilder(
					ObjectGroups.keyValueReturn("AVD_Emulator_PATH", "PRE_REQ_FOR_TEST"), "-avd",
					ObjectGroups.keyValueReturn("AVD_NAME", "PRE_REQ_FOR_TEST"));
			ProcessBuilder pbADB = new ProcessBuilder(ObjectGroups.keyValueReturn("AVD_PATH", "PRE_REQ_FOR_TEST"),
					"devices");
			Process pcADB = pbADB.start();
			InputStream is = pcADB.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			String adb = new String();
			while ((line = br.readLine()) != null) {
				if (line.length() > 0) {
					line += br.readLine();
					adb = line;
				}
			}
			if (adb.toLowerCase().contains("emulator-5554")) {
				System.out.println("AVD Device Already exists");
				System.out.println("Device is connected");
				Thread.sleep(10000);
				System.out.println("Waiting for the system to load");
			} else {
				System.out.println("Device Not connected");
				System.out.println("AVD starting");
				@SuppressWarnings("unused")
				Process pcEmulator = pbEmulator.start();
				System.out.println("AVD started");
				Thread.sleep(10000);
				System.out.println("Waiting for the system Not to load");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error in Starting AVD" + e.getMessage());
		}
	}

	public static void stopAvd() throws IOException, InterruptedException {
		try {
			for(int PORT=5554;PORT<=5556;PORT +=2) {
			ProcessBuilder pbEmulator = new ProcessBuilder(ObjectGroups.keyValueReturn("AVD_PATH", "PRE_REQ_FOR_TEST"),
					"-s", "emulator-"+PORT, "emu", "kill");
			ProcessBuilder pbADB = new ProcessBuilder(ObjectGroups.keyValueReturn("AVD_PATH", "PRE_REQ_FOR_TEST"),
					"devices");
			Process pcADB = pbADB.start();
			InputStream is = pcADB.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			String adb = new String();
			while ((line = br.readLine()) != null) {
				if (line.length() > 0) {
					line += br.readLine();
					adb = line;
				}
			}

			if (adb.toLowerCase().contains("emulator-"+PORT)) {
				System.out.println("Device is connected");
				@SuppressWarnings("unused")
				Process pcEmulator = pbEmulator.start();
				System.out.println("Emulator closed successfully");
			} else {
				System.out.println("Device Not connected");
			}}
		} catch (Exception e) {
			LOGGER.error("Error in Stoping AVD" + e.getMessage());
		}
	}
}