package com.citi.mobileAutomation.autoInvoking;

import java.io.IOException;

import io.appium.java_client.service.local.AppiumDriverLocalService;

public class OpeningAppiumServer extends Thread {
	protected static AppiumDriverLocalService service;

	public static void stopAppiumServer() {
		service = AppiumDriverLocalService.buildDefaultService();
		System.out.println("server close");
		service.stop();
	}

	public static void startAppiumServer() throws IOException, InterruptedException {
		System.out.println("starting appium server");
		service = AppiumDriverLocalService.buildDefaultService();
		service.stop();
		service.start();
		System.out.println("appium server started");
	}
}