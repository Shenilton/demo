package com.citi.mobileAutomation.countrySpecific;

public class CountryFlag_ID {
	
	public static final boolean accountDetails_100_MF = true;
	public static final boolean accountDetails_100_Savings = true;
	public static final boolean accountDetails_100_SB = true;
	public static final boolean accountDetails_100_checkings = true;
	public static final boolean accountDetails_100_checkings_iO= true;
	public static final boolean accountDetails_120_TD = true;
	public static final boolean accountDetails_120_TD_iOS = true;
	public static final boolean accountDetails_100_TimeDeposit = true;
	public static final boolean accountDetails_100_RC = true;
	public static final boolean accountDetails_100_RC_iOS = true;
	public static final boolean accountDetails_100_Savings_iOS= true;
	
	

}
