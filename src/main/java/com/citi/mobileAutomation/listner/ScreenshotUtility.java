package com.citi.mobileAutomation.listner;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.citi.mobileAutomation.controlers.Initilizer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ScreenshotUtility implements ITestListener {

	public static AppiumDriver<MobileElement> driver = (AppiumDriver<MobileElement>) Initilizer.driver;
	public static FileInputStream is = null;
	public static FileOutputStream fos = null;
	public static Xls_Reader data = null;
	public static int count = 1;


	private static String getTestMethodName(ITestResult iTestResult) {
		return iTestResult.getMethod().getConstructorOrMethod().getName();
	}

	public void onStart(ITestContext tr) {
		System.out.println("I am in onStart method " + tr.getName());
		//tr.setAttribute("AppiumDriver", Initilizer.driver);
	}

	public void onFinish(ITestContext tr) {
		System.out.println("I am in onFinish method " + tr.getName());
	}

	public void onTestSuccess(ITestResult tr) {
		System.out.println("I am in onTestSuccess method " + getTestMethodName(tr) + " succeed");
	}

	public void onTestFailure(ITestResult tr) {
	}

	public void onTestStart(ITestResult tr) {
		System.out.println("I am in onTestStart method " + getTestMethodName(tr) + " start");
		//Initilizer.logger = Initilizer.extent.startTest("To Validate " + getTestMethodName(tr));
		
	}

	public void onTestSkipped(ITestResult tr) {
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult tr) {
	}

}