package com.citi.mobileAutomation.assertEngine;

import org.testng.Assert;

public class Assert_Engine {

	public static void assertMapping(String assertTypt, String actual, String expected, boolean expectedResult) {
		if (assertTypt.equalsIgnoreCase("Equals")) {
			Assert.assertEquals(actual, expected);
		} else if (assertTypt.equalsIgnoreCase("NotEquals")) {
			Assert.assertNotEquals(actual, expected);
		} else if (assertTypt.equalsIgnoreCase("display")) {
			Assert.assertTrue(expectedResult);
		} else if (assertTypt.equalsIgnoreCase("Contains")) {
			Assert.assertTrue(actual.contains(expected));
		} else if (assertTypt.equalsIgnoreCase("TrueMessage")) {
			Assert.assertTrue(expectedResult, expected);
		}
	}
}