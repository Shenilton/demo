package com.citi.mobileAutomation.baseTest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.openqa.selenium.remote.DesiredCapabilities;

import com.citi.mobileAutomation.controlers.InitMethodWindows;
import com.citi.mobileAutomation.controlers.InitMethodiOS;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class DriversInitilization {

	protected static DesiredCapabilities caps = null;
	public static AppiumDriver<MobileElement> driver;
	public static String HK = "";
	public static String Country = "HK";
	public static String Country2 = "TH";
	static File app = null;

	public static AppiumDriver<MobileElement> initilizeAndroid(String appName) throws IOException {
		File file = new File(InitMethodWindows.ProjectWorkingDirectory + "/ListOfAPK's/" + appName + ".apk");
		DesiredCapabilities caps = new DesiredCapabilities();
		Xls_Reader read = new Xls_Reader();
		System.out.println(read.path);
		read = new Xls_Reader(read.path);
		Map<String, String> map = read.setMapData("PRE_REQ_FOR_TEST", InitMethodWindows.KEYWORDS_SG);
		caps.setCapability("deviceName", map.get("deviceName"));
		caps.setCapability("platformVersion", map.get("platformVersion"));
		caps.setCapability("platformName", map.get("platformName"));
		caps.setCapability("skipUnlock", "true");
		caps.setCapability("app", file.getAbsolutePath());
		caps.setCapability("appPackage", map.get("appPackage "));
		caps.setCapability("appActivity", map.get("appActivity "));
		caps.setCapability("androidInstallTimeout", 150000);
		caps.setCapability("autoGrantPermissions", true);
		return driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), caps);
	}

	public static AppiumDriver<MobileElement> initilizeiOS(String appName) throws IOException {
		File file = new File(InitMethodWindows.ProjectWorkingDirectory + "/ListOfAPK's/" + appName + ".app");
		DesiredCapabilities caps = new DesiredCapabilities();
		Xls_Reader read = new Xls_Reader();
		System.out.println(read.path);
		read = new Xls_Reader(read.path);
		Map<String, String> map = read.setMapData("PRE_REQ_FOR_TEST", InitMethodiOS.KEYWORDS_SG);
		caps.setCapability("deviceName", map.get("deviceNameIOS"));
		caps.setCapability("platformName", map.get("iOS"));
		caps.setCapability("platformVersion", map.get("platformVersionIOS"));
		caps.setCapability("bundleId", map.get("bundleID"));
		caps.setCapability("app", file.getAbsolutePath().toString());
		System.out.println("Shenilton 1 " + file.getAbsolutePath().toString());
		caps.setCapability("udid", map.get("udidIOS"));
		caps.setCapability("newCommandTimeout", 60 * 50);
		System.out.println(caps.toString());
		return driver = new IOSDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), caps);
	}
}